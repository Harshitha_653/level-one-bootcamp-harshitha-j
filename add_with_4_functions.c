//Write a program to add two user input numbers using 4 functions.
#include <stdio.h>
int readfloatnum();
int add(float a,float b);
void printresult(float a,float b,float sum);

int main()
{
    float a,b,sum;
	a=readfloatnum();
	b=readfloatnum();
	sum=add(a,b);
	printresult(a,b,sum);
	return 0;
}
	
int readfloatnum()
{
    float a;
	printf("enter a number \n");
	scanf("%f",&a);
	return a;
}
int add(float a,float b)
{
    float sum;
    sum=a+b;
	return sum;
}
void printresult(float a,float b,float sum)
{
     printf("%f+%f=%f",a,b,sum);
}	 