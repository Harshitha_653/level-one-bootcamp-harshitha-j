//WAP to find the distance between two points using structures and 4 functions.
#include <stdio.h>
#include <math.h>
int readintnum();
int diff(int a,int b);
int square(int sum);

int main()
{
     int x1,x2,y1,y2,sub1,sub2,pro1,pro2;
	 float result;
	 x1=readintnum();
	 x2=readintnum();
	 y1=readintnum();
	 y2=readintnum();
	 sub1=diff(x2,x1);
	 sub2=diff(y2,y1);
	 pro1=square(sub1);
	 pro2=square(sub2);
	 result=sqrt(pro1-pro2);
	 printf("the distance between given two points are %f",result);
}
int readintnum()
{
     int a;
	 printf("enter a number \n");
	 scanf("%d",&a);
	 return a;
}
int diff(int a,int b)
{
     int sub;
	 sub=a-b;
	 return sub;
}
int square(int sub)
{
    int pro;
	pro=sub*sub;
	return pro;
}	
	 