//WAP to find the volume of a tromboloid using 4 functions.
#include <stdio.h>
float input();
float computeVolume(float a,float b,float c);
void output(float result);
int main()
{
     float h,d,b,result;
	 h=input();
	 d=input();
	 b=input();
	 result=computeVolume(h,d,b);
	 output(result);
	 return 0;
}

float input()
{
      float a;
	  printf("enter the values in the order of h,d,b \n");
	  scanf("%f",&a);
	  return a;
}
float computeVolume(float a,float b,float c)
{
      float result;
	  result=0.333333*((a*b)+b/c);
	  return result;
}
void output(float result)
{
     printf("the volume of tromboloid is %f",result);
}	 