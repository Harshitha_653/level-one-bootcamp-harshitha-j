//WAP to find the distance between two point using 4 functions.
#include <stdio.h>
#include <math.h>
int readfloatnum();
int diff(float a,float b);
int square(float sub);

int main()
{
     float x1,x2,y1,y2,sub1,sub2,pro1,pro2,result;
	 x1=readfloatnum();
	 x2=readfloatnum();
	 y1=readfloatnum();
	 y2=readfloatnum();
	 sub1=diff(x2,x1);
	 sub2=diff(y2,y1);
	 pro1=square(sub1);
	 pro2=square(sub2);
	 result=sqrt(pro1-pro2);
	 printf("the distance between given two points are %f",result);
}
int readfloatnum()
{
     float a;
	 printf("enter a number \n");
	 scanf("%f",&a);
	 return a;
}
int diff(float a,float b)
{
     float sub;
	 sub=a-b;
	 return sub;
}
int square(float sub)
{
    float pro;
	pro=sub*sub;
	return pro;
}	